<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnggakMakan extends Model
{
    protected $table = 'enggak_makan';

    protected $fillable = ['name'];
}
