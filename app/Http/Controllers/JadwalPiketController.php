<?php

namespace App\Http\Controllers;

use App\{EnggakMakan, JadwalPiket};
use Illuminate\Support\Facades\DB;

class JadwalPiketController extends Controller
{
    public function index()
    {
        $this->_updateEnggakMakan();
        // jadwal piket
        $jadwal_piket = JadwalPiket::join('users', 'jadwal_piket.user_id', 'users.id')
        ->join('piket', 'jadwal_piket.piket_id', 'piket.id')
        ->select('name', 'piket', 'piket_id')->get();

        $jadwal_piket->date = date('d m Y', strtotime($jadwal_piket[0]->updated_at));

        // enggak makan
        $start = $end = date('Y-m-d');

        if(date('G') < 11) {
            $start  .= ' 00:00:00';
            $end    .= ' 10:59:59';
        }else if(date('G') >= 15) {
            $start  .= ' 15:00:00';
            $end    .= ' 23:59:59';
        } else {
            $start  .= ' 11:00:00';
            $end    .= ' 14:59:59';
        }

        $enggak_makan = EnggakMakan::whereBetween('created_at', [$start, $end])
        ->select('name')->get();

        return view('mainboard', compact('jadwal_piket', 'enggak_makan'));
    }

    public function rere($password)
    {
        if($password != 're'){
            return redirect()->route('home');
        }

        // update piket makan
        $datas = JadwalPiket::get();

        foreach($datas as $data) {
            $piket_now = $data->piket_id + 1;

            JadwalPiket::where('user_id', $data->user_id)
            ->update(['piket_id' => $piket_now < 5 ? $piket_now : 1]);
        }

        return redirect()->route('home');
    }

    public function addEnggakMakan()
    {
        if(isset($_GET['name'])) {
            $request = $_GET['name'];

            EnggakMakan::create([
                'name'  => $request
            ]);

            return response('success', 200);
        }

        return response('request get nya gk diisi', 400);
    }

    private function _updateEnggakMakan()
    {
        DB::table('enggak_makan')->whereDate('created_at', '!=', date('Y-m-d'))->delete();
    }
}
