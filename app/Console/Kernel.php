<?php

namespace App\Console;

use App\JadwalPiket;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        $schedule->call(function() {
            $hari_ini           = date('Y-m-d');
            $terakhir_piket     = JadwalPiket::find(1)->updated_at;
            $piket_berikutnya   = date('Y-m-d', strtotime($terakhir_piket .' +3days'));

            if($hari_ini == $piket_berikutnya) {
                JadwalPiket::where('user_id', '!=', 4)->update(['updated_at' => $hari_ini]);

                $datas = JadwalPiket::get();

                foreach($datas as $data) {
                    $piket_now = $data->piket_id + 1;

                    JadwalPiket::where('user_id', $data->user_id)
                    ->update([
                        'updated_at'    => $hari_ini,
                        'piket_id'      => $piket_now < 4 ? $piket_now : 1
                    ]);
                }
            }
        })->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
