<!DOCTYPE html>
<html style="font-size: 16px;">
<head>
    @include('components.head')
</head>
<body data-home-page="Home.html" data-home-page-title="Home" class="u-body">
    <div class="container-fluid">
        @include('components.main')
    </div>

    @yield('js')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>
