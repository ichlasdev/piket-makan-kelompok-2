<h2 class="font-weigth-bold text-center p-3">
    <strong>
        Piket Makan Kelompok 2
    </strong>
    <br>
    <i>
        @php
            $rules = [
                'Sunday'    => 'Minggu',
                'Monday'    => 'Senin',
                'Tuesday'   => 'Selasa',
                'Wednesday' => 'Rabu',
                'Thursday'  => 'Kamis',
                'Friday'    => "Jum'at",
                'Saturday'  => 'Sabtu',
            ];

            $time = date('l, d-m-Y');
            echo strtr($time, $rules);
        @endphp
    </i>
</h2>
<ul class="nav nav-tabs justify-content-center" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="piket-tab" data-bs-toggle="tab" data-bs-target="#piket" type="button"
            role="tab" aria-controls="piket" aria-selected="true">Jadwal Piket</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="freak-tab" data-bs-toggle="tab" data-bs-target="#freak" type="button"
            role="tab" aria-controls="freak" aria-selected="false">Yang enggak makan</button>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade show active" id="piket" role="tabpanel" aria-labelledby="piket-tab">
        @include('components.tabs.piket-tab')
    </div>
    <div class="tab-pane fade" id="freak" role="tabpanel" aria-labelledby="freak-tab">
        @include('components.tabs.freak-tab')
    </div>
</div>

@section('js')
<script>
    var d=new Date;const enggak_makan=document.getElementById("daftar_enggak_makan"),jamMakan=()=>{let e=d.getHours();const t=document.getElementById("makan");t.innerHTML=e<11?"pagi":e>=15?"malem":"siang"};document.addEventListener("DOMContentLoaded",()=>{jamMakan(),document.getElementById("tambah_enggakmakan").addEventListener("click",()=>{let e=document.getElementById("enggak_makan");if(""!=e.value){const t=new XMLHttpRequest;t.open("GET","enggakmakan?name="+e.value),t.send(),t.onload=(()=>{200==t.status?(enggak_makan.innerHTML+='<tr><td><strong><i class="bi bi-check-lg"></i> '+e.value+"</strong></td></tr>",e.value=""):document.getElementById("error_alert").removeAttribute("hidden")})}else e.classList.add("is-invalid")}),document.getElementById("enggak_makan").addEventListener("keyup",e=>{13===e.keyCode&&document.getElementById("tambah_enggakmakan").click()}),document.getElementById("enggak_makan").addEventListener("input",e=>{document.getElementById("enggak_makan").classList.remove("is-invalid")}),document.querySelectorAll(".card-detail").forEach(e=>{e.addEventListener("click",function(e){let t=this.getAttribute("data-piketId"),n=document.getElementById("piket_"+t);n.hasAttribute("hidden")?(n.removeAttribute("hidden"),n.classList.replace("animate__flipOutX","animate__flipInX")):(n.classList.replace("animate__flipInX","animate__flipOutX"),setTimeout(()=>{n.setAttribute("hidden","")},500))})})});
</script>
@endsection
