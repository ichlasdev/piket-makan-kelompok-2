// if done editing, encrypt it with https://skalman.github.io/UglifyJS-online/

// main.blade.php script
var d = new Date();
    const enggak_makan = document.getElementById('daftar_enggak_makan');

    const jamMakan = () => {
        let j = d.getHours();
        const e = document.getElementById("makan");

        if (j < 11) {
            e.innerHTML = 'pagi';
        } else if (j >= 15) {
            e.innerHTML = 'malem';
        } else {
            e.innerHTML = 'siang';
        }
    }

    document.addEventListener("DOMContentLoaded", () => {
        jamMakan();

        document.getElementById('tambah_enggakmakan').addEventListener('click', () => {
            let input = document.getElementById('enggak_makan')

            if (input.value != '') { // empty input validation
                const xhr = new XMLHttpRequest();

                xhr.open('GET', "enggakmakan?name=" + input.value);
                xhr.send();
                xhr.onload = () => {
                    if (xhr.status == 200) {
                        enggak_makan.innerHTML += '<tr><td><strong><i class="bi bi-check-lg"></i> ' + input.value + '</strong></td></tr>';

                        input.value = "";
                    } else {
                        document.getElementById('error_alert').removeAttribute('hidden');
                    }
                }
            } else {
                input.classList.add('is-invalid'); // trigger invalid bootstrap class
            }
        });

        // DOM event listener
        document.getElementById('enggak_makan').addEventListener('keyup', (e) => {
            if (e.keyCode === 13) { // 13 = key 'Enter'
                document.getElementById('tambah_enggakmakan').click();
            }
        });

        document.getElementById('enggak_makan').addEventListener('input', (e) => {
            document.getElementById('enggak_makan').classList.remove('is-invalid');
        });

        document.querySelectorAll('.card-detail').forEach(item => {
            item.addEventListener('click', function(e) {
                let piket_id = this.getAttribute('data-piketId');
                let info_job = document.getElementById('piket_'+ piket_id);

                if(info_job.hasAttribute('hidden')){
                    info_job.removeAttribute('hidden');
                    info_job.classList.replace('animate__flipOutX', 'animate__flipInX');
                }else{
                    info_job.classList.replace('animate__flipInX', 'animate__flipOutX');
                    setTimeout(() => {
                        info_job.setAttribute('hidden', '');
                    }, 500);
                }
            })
        })
    });
