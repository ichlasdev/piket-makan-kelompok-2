<div class="container p-2">
    <div class="row">
        <div class="col">
            <div class="card border-0">
                <div class="card-body">
                    <div class="alert alert-danger fade show text-center" id="error_alert" role="alert" hidden>
                        Duh 🤦‍♂️, kek nya lagi nge bug sistemnya. bilangin ke ichlas coba,
                        <br>
                        ni burgir buat kamu 🍔
                    </div>
                    <h4 class="text-center">Yang enggak makan <b id="makan"></b></h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <table class="table table-sm table-borderless" id="daftar_enggak_makan">
                                        @foreach ($enggak_makan as $orangnya)
                                        <tr><td><strong><i class="bi bi-check-lg"></i> {{ $orangnya->name }}</strong></td></tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="form-floating mb-3">
                                <input type="text" class="form-control" id="enggak_makan" name="enggak_makan" aria-describedby="enggak_makan_feedback">
                                <label for="enggak_makan">Enggak makan? masukin nama disini!</label>
                                <div id="enggak_makan_feedback" class="invalid-feedback">
                                    Jangan kosong,- diisi oi 😑
                                </div>
                            </div>
                            <button type="button" id="tambah_enggakmakan" class="btn btn-warning">Lapor</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
