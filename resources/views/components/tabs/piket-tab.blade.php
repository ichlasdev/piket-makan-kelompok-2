<div class="container p-2">
    <div class="row text-center">
        @foreach ($jadwal_piket as $data)
            <div class="col-md-3 p-2">
                <div class="card bg-grey zoom">
                    <img src="images/{{ $data->piket }}.webp" class="card-img" width="626" height="417">
                    <div class="card-img-overlay card-detail" data-piketId="{{ $data->piket_id }}">
                        <div class="container bg-white p-2" style="border-radius: 3px">
                            <h4 class="card-title"><strong>{{ $data->name }}</strong></h4>
                            <p class="card-text">{{ $data->piket }}</p>
                        </div>
                        @if($data->piket_id == 1)
                            <p id="piket_{{ $data->piket_id }}" class="card-text font-monospace text-wrap pt-3 animate__animated animate__flipOutX" hidden>
                                <span class="bg-warning">
                                    Tugasnya supplay, masak beras sama mastiin
                                    nasi siap 15 menit sebelum waktu makan.
                                    Juga punya tanggung jawab nyuci inner-pot
                                    sebelum masak nasinya
                                </span>
                            </p>
                        @elseif($data->piket_id == 2)
                            <p id="piket_{{ $data->piket_id }}" class="card-text font-monospace text-wrap pt-3 animate__animated animate__flipOutX" hidden>
                                <span class="bg-warning">
                                    Tugasnya nyiapin lauk buat makannya.
                                    Bisa beli diluar atau masak sendiri.
                                    Juga nyuplai air galon, jangan sampe kehabisan air
                                </span>
                            </p>
                        @elseif($data->piket_id == 3)
                            <p id="piket_{{ $data->piket_id }}" class="card-text font-monospace text-wrap pt-3 animate__animated animate__flipOutX" hidden>
                                <span class="bg-warning">
                                    Tugasnya nyiapin makan supaya makan
                                    siap sebelum jam makan tiba. Nanti
                                    setelah makan, pel spot bekas makannya
                                    (bagusnya si disapu juga)
                                </span>
                            </p>
                        @elseif($data->piket_id == 4)
                            <p id="piket_{{ $data->piket_id }}" class="card-text font-monospace text-wrap pt-3 animate__animated animate__flipOutX" hidden>
                                <span class="bg-warning">
                                    Nyuci nampan + segala peralatan makan / masak
                                    bekas dari kelompok 2. Kalo mau pahala lebih,
                                    piring kotor yang sakit dicuciin juga.
                                </span>
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
