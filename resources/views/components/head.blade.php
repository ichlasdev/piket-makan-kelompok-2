<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<meta name="keywords" content="Our Amazing Team">
<meta name="description" content="">
<meta name="page_type" content="np-template-header-footer-from-plugin">
<link rel="icon" type="image/png" href="images/favicon.png">
<title>Piket Makan Kelompok 2</title>
<meta name="generator" content="Nicepage 3.17.2, nicepage.com">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
<meta property="og:title" content="Home">
<meta property="og:type" content="website">
<meta name="theme-color" content="#478ac9">
<style>
    .zoom:hover {
        transform: scale(1.1);
    }
</style>
