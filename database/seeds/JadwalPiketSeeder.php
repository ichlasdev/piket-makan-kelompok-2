<?php

use App\JadwalPiket;
use Illuminate\Database\Seeder;

class JadwalPiketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JadwalPiket::create([
            'user_id'   => '1',
            'piket_id'  => '1'
        ]);
        JadwalPiket::create([
            'user_id'   => '2',
            'piket_id'  => '2'
        ]);
        JadwalPiket::create([
            'user_id'   => '3',
            'piket_id'  => '3'
        ]);
        JadwalPiket::create([
            'user_id'   => '4',
            'piket_id'  => '4'
        ]);
    }
}
