<?php

use App\Piket;
use Illuminate\Database\Seeder;

class PiketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Piket::create(['piket'    => 'Masak Beras']);
        Piket::create(['piket'    => 'Beli Makan']);
        Piket::create(['piket'    => 'Nyiapin Makan']);
        Piket::create(['piket'    => 'Cuci Nampan']);
    }
}
